#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Simple threding interface #

from threading import Thread, Event

class FalkThread(Thread):
   
    def __init__(self, func, daemon, *args, **kwargs):
        Thread.__init__(self)
        self.daemon = daemon
        self.func   = func
         
        self.args   = args
        self.kwargs = kwargs
        
        self._stop = Event()
       
    # calls the async function, don't call this function directly, use self.start() instead.
    def run(self): 
        self.func(*self.args, **self.kwargs) 
