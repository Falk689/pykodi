#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# FalkEv main executable, you should edit and run this script to start a program written with FalkEv

import argparse

from os import getcwd
from FalkPlugins import FalkPlugins

if __name__ == "__main__":
    # configuration file path
    configPath = (".config/pykodi_config", "C:/pykodi/pykodi_config") 


    # argument parser
    parser = argparse.ArgumentParser(description="Script to batch rename series folders in a kodi-friendly way.")
    parser.add_argument("-v", action="store_true", default=False,
                       dest="verbose",
                       help="prints debug info")   
    parser.add_argument("-s", type=float, default=0.5,
                       dest="sens",
                       help="multiplier for name search, a word must be at least in `episodes * sens` to appear into name suggestions (default 0.5 or a half)")   
    parser.add_argument("dir", nargs="?", default=getcwd(),
                       help="absolute or relative path of the folder to sort/rename")

    args = parser.parse_args() 


    # instance and startup the plugin system
    fp = FalkPlugins(configPath, args) 
    fp.startup()
