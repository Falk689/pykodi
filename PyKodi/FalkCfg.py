#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Simple config files parser

from os.path import join, exists, getctime, isfile, splitdrive
import configparser as cparse

from FalkCfgStore import FalkCfgStore

class FalkCfg:

    def __init__(self, homeFolder, configPath, args):
        self.configPath = None

        # homeFolder is used only on unix-like systems #
        if homeFolder != None:
            for path in configPath:
                path = join(homeFolder, path)

                if exists(path):
                    self.configPath = join(homeFolder, path)
                    break
            
            # setting up unix-like config path #
            if self.configPath == None:
                self.configPath = join(homeFolder, configPath[0])


        else:
            # we're on a windows system #
            for path in configPath:
                if exists(path):
                    self.configPath = path
                    break

            # using the windows config path #
            if self.configPath == None:
                self.configPath = configPath[1]
  
        self.storage    = {                                                                
                           "Main" : [None, join(self.configPath, "main.ini"), None]  # keys are config file section names, values are FalkCfgStore, a path to the config file and last read time
                          } 
        self.args       = args                                                               # arguments parsed from console
        self.homeFolder = homeFolder                                                         # unix home folder
        self.verbose    = False                                                              # used to easily get [Main] or [Args] verbose setting from the outside of this class
        self.name       = "FalkCfg"                                                          # report correctly event errors


    ### Config interface ###

    # check and reload the configs dynamically, only if needed #
    def reload_configs(self, event, name, groups, fbConfig, main):
        keys = []        
        g    = [name] + groups

        for group in g:
            if (group != "Main" and group != "Args" and group in self.storage) or (main and group == "Main"):
                cFile       = self.storage[group][1]
                oldEditTime = self.storage[group][2]

                if cFile != None:
                    editTime    = getctime(cFile)

                    if editTime != oldEditTime:
                        self.storage[group][2] = editTime
                        keys.append(group)

        # if some keys need refresh we reload the files #
        if len(keys) > 0:
            self.load_configs(keys, name, groups, fbConfig)

        # else we set fallback configs here #
        else:
            self.set_fallback_configs(name, groups, fbConfig)



    # loads one or more configuration files, setting the configs into the storage #
    def load_configs(self, keys, name=None, groups=None, fbConfig=None):
        if exists(self.configPath):
            cfg   = cparse.SafeConfigParser()
            cFile = self.storage["Main"][1]

            for key in keys:
                # checking if we're working on a new file #
                if self.storage[key][1] != None:
                    cFile = self.storage[key][1]

                # setting up the last edit time for current file #
                cEdit = getctime(cFile)
                self.storage[key][2] = cEdit

                if isfile(cFile):
                    cfg.read(cFile)
                    
                    secs = cfg.sections()

                    for sec in secs:
                        if sec not in self.storage:
                            self.storage[sec] = [FalkCfgStore(), cFile, cEdit]

                        elif self.storage[sec][0] == None:
                            self.storage[sec][0] = FalkCfgStore()

                        else:
                            self.storage[sec][0].config = {}

                        
                        opts = cfg.options(sec)

                        if sec == "Main":
                            # early setting of global verbosity #
                            if "verbose" in opts:
                                v = cfg.getboolean("Main", "verbose")

                                if isinstance(v, bool):
                                    self.verbose = v

                            # adding home_dir setting to main # 
                            if self.homeFolder != None:
                                mainDir = self.homeFolder

                            else:
                                mainDir = splitdrive(path)[0]

                            self.storage[sec][0].configs["home_dir"] = mainDir

                        # populating settings #
                        for opt in opts:
                            value = cfg.get(sec, opt)
                            value = self.set_value_type(cfg, sec, opt, value)
                            self.storage[sec][0].configs[opt] = value
            

            # setting up fallback config #
            if fbConfig != None and name != None:
                self.set_fallback_configs(name, groups, fbConfig)

        else:
            raise FileNotFoundError("FalkCfg configuration folder not found: ", self.configPath)




    # sets missing configs values to fallback (provided by the plugin code) #
    def set_fallback_configs(self, name, groups, fbConfig):
        for cfg in fbConfig.keys():
            if not self.is_in_configs(name, groups, cfg):
                self.storage[name][0].configs[cfg] = fbConfig[cfg]



    # return true if a given config already exists in given groups and name #
    def is_in_configs(self, name, groups, cfg):
        if name in self.storage and cfg in self.storage[name][0].configs:
            return True

        for group in groups:
            if cfg in self.storage[group][0].configs:
                return True

        return False
    


    # converts a value from str to its (hopefully) intended type #
    def set_value_type(self, cfg, sec, opt, value):
        # checking if value contains a path to home #
        try:
            if "~" in value and self.homeFolder != None: 
                value = value.replace("~", self.homeFolder)

            value = self.set_value_default_type(value, cfg, sec, opt, False)

            if isinstance(value, str):
                # checking if the user wants value to be a str, if so, don't complain #
                if (value.startswith('"') and value.endswith('"') and '",' not in value) or (value.startswith("'") and value.endswith("'") and "'," not in value):
                    value = value.strip('"')
                    value = value.strip("'")

                # checking if value looks like a list #
                elif "," in value:
                    value = value.split(",")

                    for n, v in enumerate(value):
                        value[n] = self.set_value_default_type(value[n], cfg, sec, opt)

                    # checking if value is a multiple entry dictionary key >> value, key >> value #
                    if isinstance(value[0], str) and ">>" in value[0]:
                        vlist = value
                        value = {}
                        key   = None

                        for v in vlist:
                            if ">>" in v:
                                key, val   = v.split(">>")
                                key        = key.strip()
                                val        = val.strip()
                                value[key] = val

                            elif key != None:
                                if isinstance(value[key], list):
                                    value[key].append(v.strip())

                                else:
                                    value[key] = [value[key], v]

    
                # value may still be a single entry dictionary key » value #
                elif ">>" in value:
                    vlist = value
                    value = {}
    
                    key, val = vlist.split(">>")

                    key = key.strip()
                    val = self.get_type_value(val, cfg, sec, opt) 

                    value[key] = val 
 
            return value


        except Exception as e:
            print("CONFIG ERROR: " + str(e))

            return None




    # converts value to its proper default type #
    def set_value_default_type(self, value, cfg, sec, opt, strCheck=True):
        value = value.strip()

        # checking if user wants value to be a str #
        if strCheck and (value.startswith('"') and value.endswith('"')) or (value.startswith("'") and value.endswith("'")):
            value = value.strip('"')
            value = value.strip("'")

        # checking if value is a bool #
        elif value.lower() == "true" or value.lower() == "false":
            value = cfg.getboolean(sec, opt)

        # checking if value is a none #
        elif value.lower() == "null" or value.lower() == "none":
            value = None

        # checking if value is a float #
        elif "." in value:
            try:
                value = float(value)

            except ValueError:
                pass

        # check if it's an int #
        else:
            try:
                value = int(value)

            except ValueError:
                pass

        return value



    # set plugin config path into the path dictionary #
    def set_config_path(self, name, cName):
        if name not in self.storage:
            cPath = join(self.configPath, cName)

            if isfile(cPath):
                cEdit = getctime(cPath)
                self.storage[name] = [None, cPath, None]



    # returns a dictionary containing requested settings #
    def get_settings(self, name, groups):
        result = {}

        if name in self.storage:
            result[name] = self.storage[name][0]

        for group in groups:
            if group in self.storage and group not in result:
                result[group] = self.storage[group][0]

        return result
                            


    # populating parsed arguments #
    def set_args(self):
        if self.args != None:
            self.storage["Args"] = [FalkCfgStore(), None, None]

            for key, value in vars(self.args).items():
                if key not in self.storage["Args"][0].configs:
                    # setting global verbosity level #
                    if key == "verbose":
                        self.verbose = value

                    self.storage["Args"][0].configs[key] = value

            self.args = None



    # debug function, print configs #
    def print_configs(self):
        print(self.storage)

        for name, obj in self.storage.items():
            print(name)
            print(obj[0].configs)

