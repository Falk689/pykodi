#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Simple text based GUI for KodiRenameLogic 

from os.path import abspath, basename, dirname, join, split, isfile, exists
from os import listdir, sep, getcwd
from random import seed, randrange

from FalkPluginBase import FalkPluginBase

HELP_SORT = """cmd                              -         effect
h, help                          - print this  help text
mv [e|b] [in] {e|b} [out]        - moves an episode type: `help mv` for more help.
sw [e|b] [in] {e|b} [out]        - swaps an episode with another: `help sw` for more help.
rm [e|b] [idx]                   - removes a given index
random                           - randomize episodes sorting
q,quit,exit                      - stop editing the sorting (will ask for confirmation to save)
"""

HELP_MOVE ="""Moves episodes changing sorting and organization
Usage (arguments in [] are required):
mv [e|b] [in] {e|b} [out]

examples:
mv e 0 2      - moves episode 0 from "episodes" to index 2  
mv b 7 5      - change episode 7 key from the "bonuses" list to 5 and 5 to 7 exists
mv e 0 b 5    - moves episode 0 from the "episodes" list "bonuses" giving it 5 as key
mv e 0 b OP   - moves episode 0 from the "episodes" list to "bonuses" using "OP" (opening) as key
mv b 6 e 1    - this will insert "bonuses" episode 6 to "episodes" at index 1 without swapping

"""

HELP_SWAP ="""Swaps episodes changing sorting and organization.
Important: both elements must exists in order to swap.
Usage (arguments in [] are required):
sw [e|b] [in] {e|b} [out]

examples:
sw e 0 2      - swap episode 0 from "episodes" list with episode 2 from the same list
sw b 7 5      - swap "bonuses" episode 7 with 5
sw b 5 e 4    - swap "bonues" 5 with "episodes" 4
"""

class KodiRenameGUI(FalkPluginBase):
   
    def __init__(self):
        FalkPluginBase.__init__(self)

        # common vars #
        #self.threaded = True # we probably don't want threading by now
        self.wantsCfg = True
      
        self.events   = { 
                          "on_startup"           : [],
                          "on_return_kfilelist"  : [],
                          "on_return_kname"      : []
                        }

        self.fbConfig = {
                         "dir"  : None,
                        }
      
        self.name     = "KodiRenameGUI"             # plugin name used as section name in the config file
        #self.cName    = "kodirenamegui.ini"        # config file name located into the main configuration folder

        # custom vars #
        self.fileName = None                        # base file name to use for the output
        self.fileList = None                        # local sortable filelist
        self.running  = True                        # execute mainloop while only if running
        self.season   = 1                           # season number

    ### PLUGIN METHODS ###

    # main interface loop
    def mainloop(self, event):
        self.directory = abspath(self.read_config("dir"))
        
        self.change_dir(self.directory, self.read_config("home_dir"))

        self.fireEv("on_ask_kfilelist", self.directory)

        print("\nDo you want to sort the output?", end="")

        if self.ask_confirmation():
            self.sort_output()

        self.select_season()

        self.fireEv("on_check_kname", listdir(self.directory))

        print("\nFolder: {}\nTitle: {}\nSeason: {}".format(self.directory, self.fileName, self.season))

        self.print_filelist(self.fileList)

        print("\nThis will actually rename the files, proceed?", end='')

        if self.ask_confirmation():
            self.fireEv("on_kodi_rename", self.directory, self.fileList, self.fileName, self.season)

        else:
            print("Exit...")



    # change and verify current directory, returns dir name
    def change_dir(self, target, fallback=None):
        # home symbol support
        if "~" in target:
            target = target.replace("~", self.read_config("home_dir"))

        # parent and sibling directory support
        if target.startswith(".."):
            tdir = self.directory

            for dots in target.split(sep):
                if dots == "..":
                    tdir = dirname(tdir)

                elif dots != "":
                    tdir = join(tdir, dots)
                    
            target = tdir
 
        # relative path support 
        elif dirname(target) == "":
            target = join(self.directory, target)

        # workaround self.wd not displaying correcly
        if target.endswith(sep):
            target = target[:-1]

        if exists(abspath(target)):
            self.directory = abspath(target)

            if target == self.read_config("home_dir"):
                self.wd = "~"

            else:
                self.wd = basename(target)

                if self.wd == "":
                    self.wd = "/"

        else:
            print("Directory not found: `{}`.".format(target))

            if fallback != None:
                self.directory = fallback 

                if target == self.read_config("home_dir"):
                    self.wd = "~"

                else:
                    self.wd = basename(target)

                    if self.wd == "":
                        self.wd = "/"


    # tries to get the right name and asks to the user what to use #
    def select_name(self, events, names):
        b = basename(self.directory)

        if b not in names:
            names.append(b)

        print ("Please select the right title by index or type a custom one.")

        while 1:
            for l in enumerate(names):
                print("{} -> {}".format(*l))

            cmd = input("\ntitle: ")

            try:
                n = int(cmd)

                if n < len(names):
                    self.fileName = names[n]

                else:
                    print("please select an index")


            except ValueError:
                self.fileName = cmd

                if len(cmd) == 0:
                    self.fileName = names[0]

            if len(self.fileName) > 0:
                print("\nSelected title: `{}`".format(self.fileName))

                print("Confirm?", end='')

                if self.ask_confirmation(True):
                    return

    # print the final filelist order #
    def print_filelist(self, filelist):
        print("Current filelist:")

        if len(filelist["episodes"]) == 0:
            print("Episodes: None")

        else:
            print("Episodes:")

            for n, ep in enumerate(filelist["episodes"]):
                print("{} -> {}".format(n+1, ep))

        if len(filelist["bonuses"]) == 0:
            print("Bonuses: None")

        else:
            print("Bonuses:")

            for key, item in filelist["bonuses"].items():
                print("{} -> {}".format(key, item))


    
    # change episodes sorting #
    def sort_output(self):
        plist = True
        tmplist = self.fileList.copy()

        while 1:
            if plist:
                self.print_filelist(tmplist)
            plist = True

            cmd = input("\nsort: ")

            if cmd == "h" or cmd == "help":
                print(HELP_SORT)
                plist = False

            elif cmd == "q" or cmd == "exit" or cmd == "quit":
                print("Do you want to save your edits?", end="")
                if self.ask_confirmation(True):
                    self.fileList = tmplist
                return

            elif cmd == "mv" or cmd == "sw":
                print("{0} needs more arguments. type: `help {0}` for help.".format(cmd))
                plist = False

            elif cmd == "random":
                n = len(tmplist["episodes"])

                for i in range(n*10):
                    seed()

                    idx = randrange(n)
                    out = randrange(n)

                    if out == idx:
                        out = randrange(n)

                    self.move_episode(False, tmplist["episodes"], idx, out)


            
            elif " " in cmd and len(cmd.strip()) > 1:
                cmd = cmd.split()

                if cmd[0] == "mv" or cmd[0] == "sw":
                    if len(cmd) < 3:
                        print("{0} needs more arguments. type: `help {0}` for help.".format(cmd[0]))
                        plist = False
                    
                    # move copy checks #
                    elif (cmd[1] == "e" or cmd[1] == "b") and (len(cmd) == 4 or (len(cmd) > 4 and  (cmd[3] == "e" or cmd[3] == "b"))):
                            if cmd[1] == "e":
                                try:
                                    idx = int(cmd[2])

                                    inlist = tmplist["episodes"]

                                    if len(cmd) == 4:
                                        try:
                                            out = int(cmd[3])
                                            plist = self.move_episode(cmd[0] == "mv", inlist, idx, out)

                                        except ValueError:
                                            print("{0} error. type: `help {0}` for help.".format(cmd[0]))
                                            plist = False

                                    elif cmd[3] == "e":
                                        try:
                                            out = int(cmd[4])
                                            plist = self.move_episode(cmd[0] == "mv", inlist, idx, out)

                                        except ValueError:
                                            print("{0} error. type: `help {0}` for help.".format(cmd[0]))
                                            plist = False


                                    elif cmd[3] == "b":
                                        try:
                                            out = int(cmd[4])

                                        except ValueError:
                                            out = cmd[4]

                                        plist = self.move_episode(cmd[0] == "mv", inlist, idx, out, tmplist["bonuses"])

                                    if not plist:
                                        print("{0} error. type: `help {0}` for help.".format(cmd[0]))


                                except ValueError:
                                    print("{0} error. type: `help {0}` for help.".format(cmd[0]))
                                    plist = False


                            else:
                                inlist = tmplist["bonuses"]

                                try:
                                    idx = int(cmd[2])

                                except ValueError:
                                    idx = cmd[2]

                                if len(cmd) == 4:
                                    try:
                                        out = int(cmd[3])

                                    except ValueError:
                                        out = cmd[3]

                                    plist = self.move_episode(cmd[0] == "mv", inlist, idx, out)

                                elif cmd[3] == "b":
                                    try:
                                        out = int(cmd[4])

                                    except ValueError:
                                        out = cmd[4]

                                    plist = self.move_episode(cmd[0] == "mv", inlist, idx, out)

                                elif cmd[3] == "e":
                                    try:
                                        out = int(cmd[4])
                                        plist = self.move_episode(cmd[0] == "mv", inlist, idx, out, tmplist["episodes"])

                                    except ValueError:
                                        print("{0} error. type: `help {0}` for help.".format(cmd[0]))
                                        plist = False

                                if not plist:
                                    print("{0} error. type: `help {0}` for help.".format(cmd[0]))

                    
                    else:
                        print("{0} wrong arguments. type: `help {0}` for help.".format(cmd[0]))
                        plist = False

                # remove check #
                elif cmd[0] == "rm":
                    if len(cmd) == 3:
                        if cmd[1] == "e":
                            try:
                                idx = int(cmd[2])

                                plist = self.remove_episode(self.fileList["episodes"], idx)

                            except ValueError:
                                print("{} wrong arguments. idx must be an int for `e`".format(cmd[0]))
                                plist = False
                        
                        elif cmd[1] == "b":
                            try:
                                idx = int(cmd[2])

                            except ValueError:
                                idx = cmd[2]

                            plist = self.remove_episode(self.fileList["bonuses"], idx)


                        if not plist:
                            print("{} wrong arguments.".format(cmd[0]))


                    else:
                        print("{} wrong arguments.".format(cmd[0]))
                        plist = False


                elif cmd[0] == "help":
                    if cmd[1] == "mv":
                        print(HELP_MOVE)

                    elif cmd[1] == "sw":
                        print(HELP_SWAP)

                    else:
                        print("No help text available for: `{}`.".format(cmd[1]))

                    plist = False


            elif len(cmd) > 0:
                print("Command not found: `{}`".format(cmd))
                plist = False



    # remove an episode from a list #
    def remove_episode(self, inlist, idx):
        if isinstance(inlist, list) and idx - 1 < len(inlist):
            inlist.pop(idx - 1)
            return True

        elif isinstance(inlist, dict) and idx in inlist:
            del inlist[idx]
            return True

        return False


    
    # move or swap an episode from a filelist #
    def move_episode(self, move, inlist, idx, out, outlist=None):
        # inlist check #
        if isinstance(inlist, dict) or (isinstance(inlist, list) and idx > 0 and idx - 1 < len(inlist)):
            # there's only a list #
            if outlist == None:
                if isinstance(inlist, list):
                    idx -= 1
                    out -= 1

                # move #
                if move:
                    if isinstance(inlist, list) and out <= len(inlist) and out >= 0:
                        if out == len(inlist):
                            inlist.append(inlist.pop(idx))
                        
                        else:
                            inlist.insert(out, inlist.pop(idx))
                        return True

                    elif isinstance(inlist, dict) and out not in inlist:
                        inlist[out] = inlist.pop(idx)
                        return True



                # swap #
                elif idx >= 0 and out >= 0 and idx < len(inlist) and out < len(inlist):
                    a = inlist[idx]
                    b = inlist[out]

                    inlist[idx] = b
                    inlist[out] = a
                    return True

            
            # we've got two lists #
            elif (isinstance(outlist, list) and out - 1 <= len(outlist)) or isinstance(outlist, dict):
                if isinstance(outlist, list):
                    out = max(0, out - 1)

                else:
                    idx = max(0, idx - 1)

                # move #
                if move:
                    # outlist is a list and inlist is a dict #
                    if isinstance(outlist, list) and out >= 0 and out <= len(outlist) and idx in inlist:
                        if out == len(outlist):
                            outlist.append(inlist.pop(idx))

                        else:
                            outlist.insert(out, inlist.pop(idx))

                        return True

                    # outlist is a dict and inlist is a list # 
                    elif isinstance(outlist, dict) and out not in outlist and idx >= 0 and idx < len(inlist):
                        outlist[out] = inlist.pop(idx)
                        return True


                # swap #
                elif ((isinstance(inlist, list) and idx < len(inlist)) or isinstance(inlist, dict)) and ((isinstance(outlist, list) and out < len(outlist)) or isinstance(outlist, dict)):
                    a = inlist[idx]
                    b = outlist[out]

                    inlist[idx] = b
                    outlist[out] = a
                    return True

            return False




    # select season number #
    def select_season(self):
        while 1:
            print("Insert season number (default 1): ")
            cmd = input("Season: ")

            try:
                s = int(cmd)

            except ValueError:
                s = 1

            print("Selected season: {}. Confirm?".format(s), end="")

            if self.ask_confirmation(True):
                self.season = s
                return


    # ask confirmation #
    def ask_confirmation(self, default=False):
        if not default:
            l = " (y|N) "
        
        else:
            l = " (Y|n) "

        cmd = input(l)

        if cmd.lower() == "y" or (cmd != "n" and default):
            return True

        else:
            return False

    
    # set and print filelist #
    def manage_filelist(self, event, filelist):
        self.fileList = filelist

        self.print_filelist(self.fileList)

    ### DEFAULT METHODS ###
   
    # connect local events with callbacks #
    def connect_events(self):
        self.events["on_startup"].append(self.mainloop)
        self.events["on_return_kfilelist"].append(self.manage_filelist)
        self.events["on_return_kname"].append(self.select_name)
