# PyKodi

Script based on FalkEv to batch rename season folders in a Kodi-friendly way.

## Getting started
If you're on a unix-like system, the easiest way to test this example is to move the "pykodi_config" folder in "~/.config" and "pykodi_plugins" in your home folder.

In Windows (or in case you want to change the default path) you should edit "PyKodi/PyKodi.py" to point to the location of "pykodi_config" folder, then edit "pykodi_config/main.ini" to point the "pykodi_plugins" folder.

After the paths are specified correctly, to run the program just execute "PyKodi/PyKodi.py path_to_the_folder_to_batch_rename" with python3


## FalkEv
For the main FalkEv code and a more basic example see: https://gitlab.com/Falk689/falkev
