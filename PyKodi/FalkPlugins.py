#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Simple event based plugin system built in python

from os import getenv, chdir, makedirs, listdir, getcwd
from os.path import join, exists, isdir
from sys import path as Path
from imp import reload

from FalkEv import FalkEv
from FalkCfg import FalkCfg
from FalkThread import FalkThread
from FalkGUISocket import FalkGUISocket

class FalkPlugins:
   
    def __init__(self, configPath, args): 
        self.homeFolder = getenv('HOME')
        self.configPath = configPath

        self.configs    = FalkCfg(self.homeFolder, self.configPath, args)  # Config manager instance
      
        # plugins  vars #
        self.plugins    = [{}, {}, {}]
        self.remove     = []
        
        # interface vars #
        # this class or its children shouldn't contain actual GUI code, use plugins
        self.window     = None     # main window regardless of the library used
        self.socket     = None     # the socket that hosts plugins

        # debug vars #
        self.name     = "FalkPlugins" # correctly report errors on events fired by this class

    ### Plugin Interface ###

    # startup function, called by the main executable #
    def startup(self):
        # loading configurations #
        self.configs.load_configs(("Main",))
        self.configs.set_args()
        self.verbose = self.configs.verbose 

        # instancing the main event manager #
        self.fEv = FalkEv(self.verbose)

        # connecting main events #
        self.fEv.events["on_reinit_plugins"].append(self.init_plugins)
        self.fEv.events["on_reload_cfg"].append(self.configs.reload_configs)
        self.fEv.events["on_reload_all"].append(self.configs.reload_configs)
        self.fEv.events["on_reload_all"].append(self.init_plugins)
        self.fEv.events["on_loadplugin"].append(self.load_plugin)
        self.fEv.events["on_unloadplugin"].append(self.unload_plugin)

        # initializing plugins and firing the first event #
        self.init_plugins()
        self.fEv.fire_event("on_startup") 

   

    # plugin initialization adds the plugins to the python path, handle load and unload #
    def init_plugins(self, event=None, *args, **kwargs):
        # setting up verbosity level
        pluginPath       = self.configs.storage["Plugins"][0].configs["path"]
      
        if pluginPath != None:
            # setting up the pluginPath
            if pluginPath.startswith("./"):
                pluginPath = pluginPath[2:]

            if pluginPath.startswith("~/") and self.homeFolder != None:
                pluginPath = join(self.homeFolder, pluginPath[2:])

            if not exists(pluginPath):
                makedirs(pluginPath)
         
            cwd = getcwd()
            chdir(pluginPath)

            pFolders = listdir(pluginPath)

            for pluginName in pFolders:
                if isdir(pluginName):
                    plugin = join(pluginPath, pluginName)

                    if plugin not in Path:
                        Path.append(plugin)

            chdir(cwd) # reverting previous working directory
            active = []

            # adding new plugins and setting active list #
            for key, value in sorted(self.configs.storage["Plugins"][0].configs["active"].items()):

                if not isinstance(value, list):
                    value = [value]

                for pluginName in value:
                    mod    = __import__(pluginName, fromlist=[pluginName])
                    active.append(pluginName)

                    if self.is_new(pluginName):
                        if event != None:
                            reload(mod)

                        plugin = getattr(mod, pluginName)
                        inst = plugin()

                        try:
                            key = int(key)

                            while len(self.plugins) <= key:
                                self.plugins.append({})

                            self.plugins[key][pluginName] = inst
                            #self.fEv.fire_event("on_loadplugin", inst)
                            self.load_plugin(None, inst)

                        except Exception as e:
                            print("PLUGIN ERROR {}".format(e))
                            
           
            # removing unactive plugins #
            for key, pDict in enumerate(self.plugins):
                for name, plugin in pDict.items():
                    if name not in active:
                        self.unload_plugin(None, plugin, name, key)

            for key, name in self.remove:
                del self.plugins[key][name]

            self.remove = []
                  
        else:
            print("ERROR: Plugin path not set in config file.")

    
    # check if a plugin is already instanced #
    def is_new(self, pluginName):
        for pDict in self.plugins:
            if pluginName in pDict:
                return False
 
        return True
   
         
    # actually sets a plugin into the event list and start it up for usage #
    def load_plugin(self, event, plugin):
        # connecting the plugin to the fire_event method
        plugin.fireEv = self.fEv.fire_event
               
        # connecting other plugin events #
        plugin.connect_events()

        if len(plugin.events) > 0:
            if "on_loadplugin" in plugin.events: # prevent plugins to access each other instances
                print("WARNING: preventing `{}` to connect on the private event: `on_loadplugin`.".format(plugin.name))
                del plugin.events["on_loadplugin"]

            self.fEv.add_event_func(plugin.events) 
               
        # checking if the plugin requires config #
        if plugin.wantsCfg:
            # setting up plugin config file #
            if plugin.cName != None:
                self.configs.set_config_path(plugin.name, plugin.cName)

            # loading plugin config #
            plugin.reload_config()
            plugin.configs = self.configs.get_settings(plugin.name, plugin.groups)

 
        # checking if the plugin is multithreaded #
        if plugin.threaded:
            plugin.thread = FalkThread
                                   
        # checking if the plugin is part of a gui #
        if plugin.type == "interface":

            # checking if the plugin is the root of a gui #
            if plugin.isRoot:

                # safety to load a single gui root #
                if self.window == None and self.socket == None:

                    # safety to load only properly configured plugins #
                    if  plugin.root != None and plugin.socket != None:
                        self.window = plugin.root
                        self.socket = plugin.socket

                        if not isinstance(plugin.socket, FalkGUISocket) and self.verbose:
                            print("WARNING: {}.socket doesn't extend from FalkGUISocket, this may be bad.".format(plugin.name))

                    elif self.verbose:
                        print("WARNING: root interface plugin {} doesn't have root or socket.".format(plugin.name))

                elif self.verbose:
                    print("WARNING: you can only load a single root interface plugin.")

            # this may be overrided to add functionalities to the interface #
            else:
                self.socket.add_element(plugin.root, plugin.insert)
                                 
        self.fEv.fire_event("on_pluginloaded", plugin.name)



    # unload a plugin, incomplete, not needed by now #
    def unload_plugin(self, event, plugin, name, key):
        self.fEv.rem_event_func(plugin.events)
        plugin.kill()
        self.remove.append((key, name))
        self.fEv.fire_event("on_pluginunloaded", name) 


    # debug function prints plugins #
    def print_plugin_names(self):
        print(self.configs.storage["Plugins"][0].configs["active"])  

