#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Kodi rename logic plugin, connects to the interface events and answers properly, editing filenames in a kodi-friendly way #

from os import walk, chdir, listdir, rename, getcwd
from os.path import abspath, splitext, isfile, isdir, join
from re import sub, search, IGNORECASE

from FalkPluginBase import FalkPluginBase


class KodiRenameLogic(FalkPluginBase):
   
    def __init__(self):
        FalkPluginBase.__init__(self)

        # common vars #
        #self.threaded = True # we probably don't want threading by now
        self.wantsCfg = True
      
        self.events   = { 
                          "on_ask_kfilelist"     : [],
                          "on_return_kfilelist"  : [],
                          "on_check_kname"       : [],
                          "on_return_kname"      : [],
                          "on_kodi_rename"       : [],
                          "on_pluginloaded"      : []
                        }

        self.fbConfig = {
                         "strip_symbols"  : [" "],
                         "s_string"       : "S",
                         "e_string"       : "E",
                         "bonus_season"   : 0,
                         "sens"           : 0.5,
                         "reg_ept"        :  r"(\b)(EP|BD|OVA|OAV|OAD|OP|ED|END|S|SUB|ITA)([0-9]|\b)"
                        }
      
        self.name     = "KodiRenameLogic"        # plugin name used as section name in the config file
        self.cName    = "kodirenamelogic.ini"    # config file name located into the main configuration folder


        # custom vars #
        self.regPar   = r"[\(\[\{].*?[\)\]\}]"                                        # regex to remove stuff in different type of parentheses (does not work with nested ones)
        self.regSPar  = r"[\(\[\{\}\]\)]"                                             # partial workaround for nested parentheses
        self.regEpS   = r"S[0-9]{2}E[0-9]+"                                           # remove ex. S01E12 from episodes
        self.regNum   = r"(?: )?[0-9]+"                                               # regex to remove numbers in episode names and compare the rest
        self.regSp1   = r"(?!\b)([A-Z][a-z])"                                         # first regex to split names to uppercase letters (identifies upper-lower ex: `Input[Ti]tleEX`)
        self.regSp2   = r" \1"                                                        # second regex to split names to uppercase letters (insert spaces `Input TitleEX`)
        self.regSp3   = r"(?!\b)([a-z])([A-Z])"                                       # third regex to split names to uppercase letters (identifies lower-upper: `Input Titl[eE]X`)
        self.regSp4   = r"\1 \2"                                                      # fourth regex to split names to uppercase letters (insert spaces `Input Title EX`)
        self.regEpT   = None                                                          # removes common anime stuff from the title 

        self.regSrc   = r"(S00|OVA|OAV|OAD|[0-9]+[-_\.\/]5)"                         # search for bonus or OAV episodes
        self.regOvn   = r"[0-9]+[-_\.\/]5"                                            # search for OAV episode number
        self.regOvn2  = r"([eE])([0-9]+)"                                                # search for OAV episode number

        # args vars #
        self.verbose = self.read_config("verbose")



    ### PLUGIN METHODS ###
    # gets the file list trying to identify OVA and bonus episodes#
    def get_filelist(self, event, folder):
        result = {"episodes" : [], "bonuses" : {}}
        key    = 0

        ofold = getcwd()

        if folder != abspath(folder):
            folder = abspath(folder)

        chdir(folder)

        for ep in sorted(listdir(folder)):
            if not isdir(ep):
                if search(self.regSrc, ep, IGNORECASE):
                    key += 1

                    epN = search(self.regOvn, ep)

                    if not epN:
                        epN = search(self.regOvn2, ep)

                    if epN:
                        epN = epN.group(0) 

                        if epN.lower().startswith("e"):
                            epN = epN[1:]

                        while len(epN) > 1:
                            try:
                                key = int(epN)
                                break

                            except ValueError:
                                epN = epN[:-1]

                    result["bonuses"][key] = ep

                else:
                    result["episodes"].append(ep)

        self.fireEv("on_return_kfilelist", result)

        chdir(ofold)



    # gets and returns a list of possible names for this series #
    def get_name(self, event, filelist):
        namelist = []

        if self.regEpT == None:
            self.regEpT = self.read_config("reg_ept")

        for fname in filelist:
            if not fname.startswith("."):
                while "." in fname and fname.index(".") > int(len(fname) / 3):
                    fname = splitext(fname)[0]

            fname = sub(self.regPar,  '', fname)
            fname = sub(self.regEpS,  '', fname, flags=IGNORECASE)
            fname = sub(self.regSPar, '', fname)
            fname = sub(self.regNum,  '', fname)

            for s in self.read_config("strip_symbols"):
                fname = fname.strip(s).replace(s, " ")

            # adding spaces before uppercase letters
            fname = sub(self.regSp1, self.regSp2, fname)
            fname = sub(self.regSp3, self.regSp4, fname)

            fname = sub(" +", " ", fname) # removing multiple spaces

            fname = sub(self.regEpT,  '', fname, flags=IGNORECASE).strip()

            if len(fname) > 0 and fname not in namelist:
                namelist.append(fname)

        sens     = int(len(namelist) * self.read_config("sens"))
        wordlist = namelist[0].split()
        endlist  = []
        tmplist  = {}

        i        = 0
        oldword  = wordlist[0]

        # initial title check, checking common words from the first episode #
        for word in wordlist:
            i = 0
            for fname in namelist[1:]:
                if word.lower() in fname.lower().split():
                    i += 1

            if i >= sens:
                if i not in tmplist.keys():
                    tmplist[i] = [word,]

                else:
                    tmplist[i].append(word)


        wordlist = namelist[0].lower().split()

        # additional check on other episodes, to see if the first episode wasn't the common one #
        for fname in namelist[1:]:
            words = fname.split()

            for word in words:
                i = 0
                lword = word.lower()

                if lword not in wordlist:
                    wordlist.append(lword)

                    for bname in namelist:
                        if bname != fname:
                            check = bname.lower().split()

                            if lword in check:
                                i += 1

                    if i >= sens:
                        if i not in tmplist.keys():
                            tmplist[i] = [word,]

                        else:
                            tmplist[i].append(word)
        
        # adding possible titles to the final list #
        if len(tmplist) > 0:
            i = 0

            for key, item in sorted(tmplist.items(), reverse=True):
                if i == 0: 
                    fstring = None

                else:
                    fstring = endlist[i-1]

                i += 1
                    

                for word in item:
                    if fstring == None:
                        fstring = word

                    else:
                        fstring += " {}".format(word)

                
                if fstring not in endlist:
                    endlist.append(fstring)

             
        self.fireEv("on_return_kname", endlist)


    # all green: rename stuff in a proper way #
    def kodi_rename(self, event, folder, filelist, name, season):
        ep = 1

        if len(filelist["episodes"]) + len(filelist["bonuses"]) < 100:
            fill = 2

        else:
            fill = len(str(len(filelist["episodes"]) + len(filelist["bonuses"])))

        for fn in filelist["episodes"]:
            episode = str(ep).zfill(fill)
            ext     = splitext(fn)[1]
            out = "{} {}{:02d}{}{}{}".format(name,
                                             self.read_config("s_string"),
                                             season,
                                             self.read_config("e_string"),
                                             episode,
                                             ext)
 

            if fn != out:
                ulist = listdir(folder)
                # we found the same output name in the filelist but it's not this file
                if out in ulist:
                    self.tmp_rename(folder, out, ulist, filelist, ext)

                print("Renaming: {}\nin: {}".format(fn, out)) 
                rename(join(folder, fn), join(folder, out))

            ep += 1

        season = self.read_config("bonus_season")

        for key, item in filelist["bonuses"].items(): 
            ext = splitext(item)[1]

            if isinstance(key, int):
                episode = str(key).zfill(fill)
                out     = "{} {}{:02d}{}{}{}".format(name,
                                                     self.read_config("s_string"),
                                                     season,
                                                     self.read_config("e_string"),
                                                     episode,
                                                     ext)


            else:
                out = "{} {}{}".format(name, key, ext)



            if item != out:
                ulist = listdir(folder)

                # we found the same output name in the filelist but it's not this file
                if out in ulist:
                    self.tmp_rename(folder, out, ulist, filelist, ext, key)

                print("Renaming: {}\nin: {}".format(item, out)) 
                rename(join(folder, item), join(folder, out))



    # rename a file to a tmp name to prevent weird shit from happening #
    def tmp_rename(self, folder, out, ulist, filelist, ext, dkey=None):
        n = 0
        tmpname = "pykoditmp{}{}".format(n, ext)

        # check to see if we used that tmpname already
        while tmpname in ulist:
            n += 1
            tmpname = "pykoditmp{}{}".format(n, ext)

        if self.verbose:
            print("Renaming to tmpfile: {}\nin: {}".format(out, tmpname)) 

        rename(join(folder, out), join(folder, tmpname))

        # renaming the entry into the filelist
        if out in filelist["episodes"]:
            idx = filelist["episodes"].index(out)

            filelist["episodes"][idx] = tmpname

        elif dkey != None and dkey in filelist["bonuses"]:
            filelist["bonuses"][dkey] = tmpname

        else:
            for key, value in filelist["bonuses"].items():
                if value == out:
                    filelist["bonuses"][key] = tmpname
                    break



    # setting verbosity level # 
    def set_settings(self, event, name):
        if name == self.name:
            self.verbose = self.read_config("verbose")


    ### DEFAULT METHODS ###

    # connect local events with callbacks #
    def connect_events(self): 
        self.events["on_check_kname"].append(self.get_name)
        self.events["on_ask_kfilelist"].append(self.get_filelist)
        self.events["on_kodi_rename"].append(self.kodi_rename)
        self.events["on_pluginloaded"].append(self.set_settings)

